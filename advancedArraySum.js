const arr1 = [10000, 15000, 100, 200, 5]                     // { max: 15000, quantity: 1, min: 5 }
const arr2 = [8000, 7500, 8000, 200, 8000]                    // { max: 8000, quantity: 3, min: 200 }
const arr3 = [90, 91, 84, 200000, 200000, 6, 1, 92, 92, 1]    // { max: 200000, quantity: 2, min: 84 }  
const arr4 = [84, 72, 91, 100100, 91, -1, 0.5]               // { max: 100100, quantity: 1, min: 72 }

function advancedArraySum(arr) {

    var obj = {};
    obj.max = Math.max.apply(null, arr);
    obj.quantity = 0;
    obj.min = 0;


    var arrClean =[]


    for (let i = 0; i < arr.length; i++) {
        if (arr[i] == obj.max) {
            obj.quantity++;
        }

        if (arr[i] % 1 == 0) {
            if (arr[i] > 0 && arr[i] <= 100000) {
                arrClean.push(arr[i]);
            }
        }
    }

    var topValues = arrClean.sort((a, b) => b - a).slice(0, 5);

    for (let i = 0; i < topValues.length; i++) {
        if (topValues[i] % 1 == 0) {
            if (topValues[i] < 0) {
                topValues.splice(topValues[i], 1);
            }
        }
        else {
            topValues.splice(topValues[i], 1);
        }
    }

    obj.min = Math.min.apply(null, topValues);

    return obj;
}

console.log(advancedArraySum(arr1));
console.log(advancedArraySum(arr2));
console.log(advancedArraySum(arr3));
console.log(advancedArraySum(arr4));


