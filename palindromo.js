function isPalindrome(str) {
    var re = /[^A-Za-z0-9]/g;
    str = str.toLowerCase().replace(re, '');
    var len = str.length;
    for (var i = 0; i < len / 2; i++) {
        if (str[i] !== str[len - 1 - i]) {
            return false;
        }
    }
    return true;
}

console.log(isPalindrome('Atar a la rata'));   // true
console.log(isPalindrome('Ella te da detalle'));   // true
console.log(isPalindrome('Hola mundo'));   // false
console.log(isPalindrome('Saca tus butacas'));   // true -> ojo:no es palindromo




