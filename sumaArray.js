const arr1 = [1, 2, 3]                // return 6
const arr2 = [1, 200, 1]              // return 2
const arr3 = [2, -3, 4]               // return 6
const arr4 =  [1, 'a', 2, 3, '4']      // return 10

function simpleArraySum(arr) {
    let sum = 0;
    
    for (let i = 0; i < arr.length; i++) {
        if(isNaN(arr[i]) === false){
            if(arr[i] > 0 && arr[i] <= 100){
                sum += parseInt(arr[i]);
            }
        }
    }
    return sum;
}

console.log(simpleArraySum(arr1));
console.log(simpleArraySum(arr2));
console.log(simpleArraySum(arr3));
console.log(simpleArraySum(arr4));


